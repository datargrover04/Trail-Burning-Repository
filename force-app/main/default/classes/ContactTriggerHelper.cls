public class ContactTriggerHelper {
    public static void updateContactChildRollup(Set<Id> accountIdSet){
        Account accObj;
        List<Account> updatedAccountObj = new List<Account>();
        for(AggregateResult arIterator : [SELECT Count(ID)contactCount,AccountId FROM Contact WHERE AccountId IN : accountIdSet GROUP BY AccountId]){
            accObj = new Account();
            accObj.Id = String.valueOf(arIterator.get('AccountId'));
            accObj.Contact_Count__c = Integer.valueOf(arIterator.get('contactCount'));
            updatedAccountObj.add(accObj);
        }
        if(updatedAccountObj!= null && !updatedAccountObj.isEmpty()){
        update updatedAccountObj;
        }
    }
    
}