/**
 *  Class Name : CaseHierarchyWrapper
 *  Description : Wrapper class for case hierarchy 
 *  Author : Datar Grover
 *  *********************************************************
 *  Modification Log 
 *  28/06/21 - Initial Creation - Datar Grover
 * *********************************************************
 */
public with sharing class CaseHierarchyWrapper {
    
    @AuraEnabled public List<Case> caseRecordList;
    @AuraEnabled public List<GridColumnWrapper> gridColumnList;
    public CaseHierarchyWrapper(List<Case> caseRecordList,List<GridColumnWrapper> gridColumnList) {
        this.caseRecordList = caseRecordList ;
        this.gridColumnList = gridColumnList;
    }
/**
 *  Class Name : GridColumnWrapper
 *  Description : Wrapper class for grid column
 *  Author : Datar Grover
 *  *********************************************************
 *  Modification Log 
 *  28/06/21 - Initial Creation - Datar Grover
 * *********************************************************
 */
    public class GridColumnWrapper{
        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        @AuraEnabled public String type;
        @AuraEnabled public boolean isHyperLink;
        @AuraEnabled public String referenceFieldName;
        public GridColumnWrapper(String label,String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
        }
    }
}