public class ContactTriggerHandler {
    public static void afterInsertContact(List<Contact> sObjectList,Map<Id,Contact> sObjectNewMap){
        Set<Id> accountIdSet = new Set<Id>();
        for(Contact contactIterator : sObjectList){
            if(contactIterator.AccountId != null){
                accountIdSet.add(contactIterator.AccountId);
            }
        }
        ContactTriggerHelper.updateContactChildRollup(accountIdSet);
    }
    public static void afterUpdateContact(List<Contact> sObjectList,Map<Id,Contact> sObjectNewMap,Map<Id,Contact> sObjectOldMap){
        Set<Id> accountIdSet = new Set<Id>();
        for(Contact contactIterator : sObjectList){
            if(sObjectNewMap.get(contactIterator.Id).AccountId != 
               sObjectOldMap.get(contactIterator.Id).AccountId){
                   if(sObjectOldMap.get(contactIterator.Id).AccountId != null){
                      accountIdSet.add(sObjectNewMap.get(contactIterator.Id).AccountId);
                      accountIdSet.add(sObjectOldMap.get(contactIterator.Id).AccountId);
                   }else{
                       accountIdSet.add(sObjectNewMap.get(contactIterator.Id).AccountId);
                   } 
               }
        }
        ContactTriggerHelper.updateContactChildRollup(accountIdSet);
    }
    public static void afterDeleteContact(List<Contact> sObjectList,Map<Id,Contact> sObjectOldMap){
        Set<Id> accountIdSet = new Set<Id>();
        for(Contact contactIterator : sObjectList){
           accountIdSet.add(contactIterator.AccountId);
        }
        ContactTriggerHelper.updateContactChildRollup(accountIdSet);
    }
    public static void afterUndeleteContact(List<Contact> sObjectList,Map<Id,Contact> sObjectNewMap){
        Set<Id> accountIdSet = new Set<Id>();
        for(Contact contactIterator : sObjectList){
           accountIdSet.add(contactIterator.AccountId);
        }
        ContactTriggerHelper.updateContactChildRollup(accountIdSet);
    }
}