/**
 *  Class Name : CaseHierarchyController
 *  Description : Controller for showing child records as per parent case Id
 *  Author : Datar Grover
 *  *********************************************************
 *  Modification Log 
 *  27/06/21 - Initial Creation - Datar Grover
 * *********************************************************
 */

public with sharing class CaseHierarchyController {
/**
 *  Method Name : getCaseHierarchyRecord
 *  Description : Get case hierechy of select case or toggled case Id
 *  Parameters  : Stirng
 *  Return Type : List<Case>
 */
    @AuraEnabled(cacheable = true) 
    public static CaseHierarchyWrapper getCaseHierarchyRecord(String parentCaseId){
    try{
        List<Case> childCaseList = new List<Case>();
        List<Schema.FieldSetMember> caseFieldSetList = Schema.SObjectType.Case.fieldSets.Case_Field_Set.getFields();
        String caseDynamicQuery = buildDynamicQuery(caseFieldSetList,parentCaseId);
        if(caseDynamicQuery != null && !String.isBlank(caseDynamicQuery)){
            if(Schema.sObjectType.Case.isAccessible()){
            childCaseList = Database.query(String.escapeSingleQuotes(caseDynamicQuery));
            }
        }
        if(childCaseList != null && !childCaseList.isEmpty()){
            return new CaseHierarchyWrapper(childCaseList,buildTreeGridDetails(caseFieldSetList));
        }else{
            return null;
        }
      } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
      }
    }
/**
 *  Method Name : buildDynamicQuery
 *  Description : Building dynamic query from the field set
 *  Parameters  : List<Schema.FieldSetMember>,String
 *  Return Type : String
 */
    public static String buildDynamicQuery(List<Schema.FieldSetMember> caseFieldSet,String parentCaseId){
        String caseDynamicQuery = 'SELECT ID,ParentId,(SELECT ID FROM Cases)';
        String whereClause = ' FROM Case WHERE ParentId =: parentCaseId WITH SECURITY_ENFORCED ';  
        if(caseFieldSet != null){
            for(Schema.FieldSetMember fieldIterator : caseFieldSet){
                if(String.valueof(fieldIterator.getType()) == 'REFERENCE')
                {
                    String referenceFieldName = getReferenceFieldName(fieldIterator);
                    caseDynamicQuery+= ','+referenceFieldName;
                }else{
                caseDynamicQuery+= ','+fieldIterator.getFieldPath();
                }
            }
            caseDynamicQuery+=whereClause;
            return caseDynamicQuery;
        }else{
            return null;
        }
    }
/**
 *  Method Name : buildTreeGridDetails
 *  Description : Building data for tree grid
 *  Parameters  : List<Schema.FieldSetMember>,List<childCaseList>
 *  Return Type : CaseHierarchyWrapper
 */
  public static List<CaseHierarchyWrapper.GridColumnWrapper> buildTreeGridDetails(List<Schema.FieldSetMember> caseFieldSet){
    List<CaseHierarchyWrapper.GridColumnWrapper> caseGridColumnList = new List<CaseHierarchyWrapper.GridColumnWrapper>();
    for(Schema.FieldSetMember fieldIterator : caseFieldSet){
        if(Constants.HYPERLINKSET.contains(fieldIterator.getFieldPath())){
            CaseHierarchyWrapper.GridColumnWrapper gridColumnObj = new CaseHierarchyWrapper.GridColumnWrapper(fieldIterator.getLabel(),fieldIterator.getFieldPath(),'button');
            gridColumnObj.isHyperLink = true;
            caseGridColumnList.add(gridColumnObj);
        }else if(Constants.REFERENCE_FIELD_SET.contains(fieldIterator.getFieldPath())){
            CaseHierarchyWrapper.GridColumnWrapper gridColumnObj = new CaseHierarchyWrapper.GridColumnWrapper(fieldIterator.getLabel(),fieldIterator.getFieldPath(),String.valueOf(fieldIterator.getType()));
            gridColumnObj.isHyperLink = true;
            gridColumnObj.referenceFieldName = getReferenceFieldName(fieldIterator);
            caseGridColumnList.add(gridColumnObj);
        }
        else{
            CaseHierarchyWrapper.GridColumnWrapper gridColumnObj = new CaseHierarchyWrapper.GridColumnWrapper(fieldIterator.getLabel(),fieldIterator.getFieldPath(),String.valueOf(fieldIterator.getType()));
            caseGridColumnList.add(gridColumnObj);
        }
    }
    return caseGridColumnList;
  }
/**
 *  Method Name : getReferenceFieldName
 *  Description : Get reference field name from object
 *  Parameters  : Schema.FieldSetMember
 *  Return Type : String
 */
    public static String getReferenceFieldName(Schema.FieldSetMember fieldName){
        Boolean customField = fieldName.getSObjectField().getDescribe().isCustom(); // Custom field irrespective if that is custom object or standard
        String relationShipObject = fieldName.getSObjectField().getDescribe().getRelationshipName();
        String referenceApiName;
        if(customField){
            referenceApiName = relationShipObject.replace('__c', '__r')+'.Name';
            
        }else{
            referenceApiName = relationShipObject+'.Name';
        }
        return referenceApiName;
    }
}