trigger ContactTrigger on Contact (before insert ,after insert,after update,after delete,after undelete) {
     
   if(Trigger.isAfter){
       if(Trigger.isInsert){
           ContactTriggerHandler.afterInsertContact(Trigger.new, Trigger.newMap);
       }
       if(Trigger.isUpdate){
           ContactTriggerHandler.afterUpdateContact(Trigger.new, Trigger.newMap, Trigger.oldMap);
       }
       
       if(Trigger.isDelete){
           ContactTriggerHandler.afterDeleteContact(Trigger.old, Trigger.oldMap);
       }
       if(Trigger.isUndelete){
         ContactTriggerHandler.afterUndeleteContact(Trigger.new, Trigger.newMap);
       }
   }
}