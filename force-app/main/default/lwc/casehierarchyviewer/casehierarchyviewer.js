import { LightningElement,api, wire,track } from 'lwc';
import { getRelatedListRecords } from 'lightning/uiRelatedListApi';
import nochildRecord from '@salesforce/label/c.No_Data_Show';
import { NavigationMixin } from 'lightning/navigation';
import getGridColumn from '@salesforce/apex/CaseHierarchyController.getGridColumnWrapper';
import { refreshApex } from '@salesforce/apex';


export default class Casehierarchyviewer extends NavigationMixin(LightningElement) {
    label = {
        nochildRecord,
      };
      @api recordId;
      @api childCaseMap;
      isLoading = true;
      noChildData = false;
      erroredServerCall = false;
      @api errorMessage;
      gridColumns;
      gridLoadingState = false;
      @api gridData=[];
      @api childData ={};
      @api firstCall = false;
      @api fieldList;      
      @api gridColumnList;
      fieldarray;
      @api caseListResponse=[];
      @wire(getRelatedListRecords, {
        parentRecordId: '$recordId',
        relatedListId: 'Cases',
        fields: '$fieldarray',
       })caseList( { data, error }){
        if(data){
            this.caseListResponse = data;
            console.log('here--->'+JSON.stringify(data));
            console.log('fieldarray--->'+this.fieldarray);
            if(this.fieldarray){
            if(data.records){
                if(data.fields){
                    this.isLoading = false;
            if(!this.firstCall){
                this.getChildRecord(true,data);
            }else{
               
                this.firstCall = true;
                let caseData = [...data];
                this.getChildRecord(false,data,this.reocrdId);
                const updatedData = this.addChildrenToRow(
                    this.gridData,
                    this.reocrdId,
                    caseData
                );
                this.gridData = updatedData;
                }
            }
        }
        }
        }if(error){
            console.log('here error--->'+JSON.stringify(error));
            this.erroredServerCall = true;
            this.isLoading = false;
        }
      }
      buildCaseDetailGrid(data){
        let gridData =[];
            data.forEach(element =>{
            let caseObj = Object.assign({}, element);
            //if(element.Has_Child__c){
            caseObj._children = [];
            //}
            gridData.push(caseObj);
        })
        return gridData;
    }
    handleRowToggle(event) {
        const rowId = event.detail.name;
            this.gridLoadingState = true;
            this.retrieveUpdatedData(rowId).then((newData) => {
                this.gridData = newData;
            });
    }
    handleClick(event){
        const rowId = event.detail.name;
        console.log()
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: rowId,
                    actionName: 'view'
                }
            });
    }
    retrieveUpdatedData(rowId) {
        return new Promise((resolve) => {
                getCaseHierarchyRecord({'parentCaseId' : rowId}).then((result) =>{
                let caseData = [];
                caseData =this.buildCaseDetailGrid(result.caseRecordList);
                this.buildChildData(caseData,rowId);
                const updatedData = this.getChildRecord(false,result,rowId);
                resolve(updatedData);
                });
        });
    }
        addChildrenToRow(data, rowId) {
            let newTree = [];
            data.forEach(element =>{
                let hasChildrenContent = false;
                if (element.hasOwnProperty('_children') &&
                Array.isArray(element._children) &&
                element._children.length > 0) {
                  hasChildrenContent = true;
                }
                if(element.Id === rowId){
                    element._children = this.childCaseMap[element.Id] ;
                }else if(hasChildrenContent){
                        this.addChildrenToRow(element._children, rowId);
                }
                newTree.push(element);
            })
        return newTree;
    }
    buildChildData(caseData,rowId){
        if(!this.childCaseMap){
            this.childCaseMap ={};
            this.childCaseMap[rowId] = caseData;
        }else{
        this.childCaseMap[rowId] = caseData;
        }
    }
    handleRowAction(event){
        const row = event.detail.row;
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: row.Id,
                        actionName: 'view'
                    }
                });
    
    }
    getChildRecord(initialCall,data,rowId){
        if(initialCall){
            if(data){
            this.buildChildData(data.records,this.recordId);
            this.gridData =  this.buildCaseDetailGrid(data.records);
           }else{
              this.noChildData = true;
           }
              this.isLoading = false;
           }else{
            let caseData = [];
            caseData =this.buildCaseDetailGrid(data.caseRecordList);
            this.buildChildData(caseData,rowId);
            const updatedData = this.addChildrenToRow(
                this.gridData,
                rowId,
                caseData
            );
            this.gridLoadingState = false;
            return updatedData;
        }
    }
    @wire(getGridColumn)gridList({data,error}){
        if(data){
           let refreshedColumnList=[];
           let columnFieldList = [];
           data.forEach(element =>{
               let columnObj = Object.assign({}, element);
               if(columnObj.isHyperLink === true){
                   columnObj.typeAttributes = 
                    { label: { fieldName: columnObj.fieldName }, variant: 'base' }
               }
               columnFieldList.push(columnObj.fieldName);
               refreshedColumnList.push(columnObj);
               
           });
               console.log('columnFieldList'+JSON.stringify(columnFieldList));
               this.fieldarray = columnFieldList;
               this.gridColumns =refreshedColumnList; 
               this.getLatest();
            
        }if(error){
            console.log('error here');
        }
      }
      getLatest() {
        refreshApex(this.caseListResponse)
            .then(()=> {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Refreshed Data',
                        variant: 'success'
                    })
                );
            })
            .catch((error) => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error Refreshing Data',
                        message: message,
                        variant: 'error'
                    })
                );
            });
        }
        
}